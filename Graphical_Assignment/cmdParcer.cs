﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Graphical_Assignment
{
    class cmdParcer
    {
       
        String Error;
        
        Boolean error = false;
        Brush br;
        Graphics g;


        int height, width, radious, squaresize;
        Boolean method;

        Bitmap bitmapvalue = new Bitmap(600, 990);
       
        public cmdParcer()
        {
            bitmapvalue = new Bitmap(900, 700);
            g = Graphics.FromImage(bitmapvalue);
        }

        //function to check each line 
        public string checkLines(string code, int line)
        {
            int temp;

            char[] code_delimiters = new char[] { ' ' };
            String[] words = code.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);

            //filter word in a line
            if (words[0] == "draw")
            {
                if (words[1] == "circle")
                {
                    if (!(words.Length == 3))
                    {
                        error = true;
                        Error = "The error in on line " + line + " please type correct code for 'draw circle'";
                        
                        return Error;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        Circle circle = new Circle();
                        circle.SetColor(fill);
                        circle.SetX(moveX);
                        circle.SetY(moveY);
                        circle.SetRadious(Convert.ToInt32(words[2]));
                        circle.Draw(g, c, thickness, br);
                        //return null;
                    }
                    else if (words[2] == "radious")
                    {
                        Circle circle = new Circle();
                        circle.SetColor(fill);
                        circle.SetX(moveX);
                        circle.SetY(moveY);
                        circle.SetRadious(this.radious);
                        circle.Draw(g, c, thickness, br);

                    }

                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;

                    }
                }

                else if (words[1] == "square")
                {
                    if (!(words.Length == 3))
                    {
                        error = true;
                        Error = "The error in on line " + line + " Please type correct code for 'draw square'";
                        
                        return Error;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        Square square = new Square();
                        square.SetColor(fill);
                        square.SetX(moveX);
                        square.SetY(moveY);
                        square.SetSquare(Convert.ToInt32(words[2]));
                        square.Draw(g, c, thickness, br);
                        //return null;
                    }
                    else if (words[2] == "squaresize")
                    {
                        Square square = new Square();
                        square.SetColor(fill);
                        square.SetX(moveX);
                        square.SetY(moveY);
                        square.SetSquare(this.squaresize);
                        square.Draw(g, c, thickness, br);

                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;

                    }
                }

                else if (words[1] == "rectangle")
                {
                    if (!(words.Length == 4))
                    {
                        error = true;
                        Error = "The error in on line " + line + " Please type correct code for 'draw rectangle'";
                        

                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp))
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.SetColor(fill);
                        rectangle.SetX(moveX);
                        rectangle.SetY(moveY);
                        rectangle.setHeight(Convert.ToInt32(words[2]));
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.Draw(g, c, thickness, br);
                        //return null;
                    }
                    else if ((words[2] == "height" || words[2] == "width") && (words[3] == "height" || words[3] == "width"))
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.SetColor(fill);
                        rectangle.SetX(moveX);
                        rectangle.SetY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(width);
                        rectangle.Draw(g, c, thickness, br);
                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else if (words[1] == "triangle")
                {
                    if (!(words.Length == 8))
                    {
                        error = true;
                        Error = "The error in on line " + line + " Please type correct code for 'draw triangle'";
                        

                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp) && Int32.TryParse(words[4], out temp) && Int32.TryParse(words[5], out temp) && Int32.TryParse(words[6], out temp) && Int32.TryParse(words[7], out temp))
                    {
                        Triangle triangle = new Triangle();
                        triangle.SetColor(fill);
                        triangle.setPoints(Convert.ToInt32(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]), Convert.ToInt32(words[7]));
                        triangle.Draw(g, c, thickness, br);
                        //return null;
                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else
                {
                    MessageBox.Show("Please enter correct code in line " + line);
                    return "Please enter correct code in line " + line;
                }
            }
            else if (words[0] == "radious")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.radious = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.radious = radious + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter Correct Parameters");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "height")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.height = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.height = height + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter Correct Parameters");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "width")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.width = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.width = width + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter Correct Parameters");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "squaresize")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.squaresize = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.squaresize = squaresize + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter Correct Parameters");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "moveto")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    Error = "The error in on line " + line + " Please type correct code for 'move'";
                    
                    return Error;
                }
                else if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[2], out temp))
                {
                    this.moveX = Convert.ToInt32(words[1]);
                    this.moveY = Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("Please enter The parameters in integers in line " + line);
                    return "Please enter The parameters in integers in line " + line;

                }
            }
            else if (words[0] == "color")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    Error = "The error in on line " + line + " Please type correct code for 'move'";
                   
                    return Error;
                }
                else if (Int32.TryParse(words[2], out temp))
                {
                    this.thickness = Convert.ToInt32(words[2]);
                    Color cs = Color.FromName(words[1]);

                    if (cs.IsKnownColor)
                    {
                        this.c = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                        //return "color of pen changed to " + char.ToUpper(words[1][0]) + words[1].Substring(1);
                    }
                    else
                    {
                        MessageBox.Show("please enter a valid color");  
                        return ("please enter a valid color");
                    }
                }
                else
                {
                    MessageBox.Show("Please enter The parameters in integers" + line);
                    return "Please enter The parameters in integers" + line;

                }
            }
            else if (words[0] == "fill")
            {
                if (!(words.Length == 2))
                {
                    error = true;
                    Error = "The error in on line " + line + " Please type correct code for 'move'";
                   
                    return Error;
                }

                else
                {
                    Color fil = Color.FromName(words[1]);
                    if (fil.IsKnownColor)
                    {
                        this.fill = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                    }
                    else if (words[1] == "no")
                    {
                        this.fill = Color.Transparent;

                    }
                    else { 

                        return ("please enter a valid color");
                    }
                }
            }
            else if (words[0] == "loop")        //for loop condition 
            {
                this.loopStart = true;
                this.loopSize = Convert.ToInt32(words[1]);
            }
            else if (words[0] == "endloop")
            {
                loopStart = false;
                loopSize = 0;
            }
            else if (words[0] == "if")
            {
                Boolean loopStart = false;
                if (words[1].ToLower().Equals("radius"))
                {
                    if (radious == int.Parse(words[3]))
                    {
                        loopStart = true;
                    }
                }
                else if (words[1].ToLower().Equals("width"))
                {
                    if (width == int.Parse(words[3]))
                    {
                        loopStart = true;
                    }
                }
                else if (words[1].ToLower().Equals("height"))
                {
                    if (height == int.Parse(words[3]))
                    {
                        loopStart = true;
                    }
                }

            }
            else if (words[0] == "endif")
            {
                loopStart = false;
                loopSize = 0;
            }
            else
            {
                MessageBox.Show("Enter correct code for line 1");
                return "Please enter The correct codes";
            }

            return null;
        }

        List<string> loopContent;

        

        Color c = Color.Black;
        Color fill = Color.Transparent;
        int moveX = 0, moveY = 0;
        int thickness = 3;

      
        int loopSize;
        Boolean loopStart = false;

       
        public String ParserClass(string command, string code)
        {

            string comm = command.ToLower();

            switch (command)
            {
                case "run":
                    try
                    {
                        char[] delimiters = new char[] { '\r', '\n' };
                        string[] parts = code.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        loopContent = new List<string>();
                        for (int i = 0; i < parts.Length; i++)
                        {

                            if (this.loopStart == true)
                            {
                                loopContent.Add(parts[i]);
                            }
                            else
                            {

                                checkLines(parts[i], i + 1);
                            }
                        }

                        for (int k = 0; k < loopSize; k++)
                        {
                            for (int j = 0; j < loopContent.Count - 1; j++)
                            {
                               
                                checkLines(loopContent[j], k);
                            }

                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        return ex.Message;

                    }
                    catch (FormatException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    break;
                case "clear":
                    
                    break;
                case "reset":
                    this.moveX = 0;
                    this.moveY = 0;
                    this.thickness = 2;
                    this.fill = Color.Transparent;
                    c = Color.Black;
                    return "Pen reset to default value";
                case "help":
                    return "For Your Help:\n" +
                             "draw circle 100\n" +
                             "draw rectangle 100 50\n" +
                             "draw triangle 10 10 100 10 50 60\n" +
                             "draw square 50\n" +
                             "move 100 100\n" +
                             "color red 23\n" +
                             "fill red\n" +
                             "fill no\n";
                default:
                    MessageBox.Show("Please enter a correct command");
                    return "Please enter a command";

            }

            return null;



        }
        public Bitmap GetBitmap()
        {
          return this.bitmapvalue;

        }
        /*public string Variable(string command, string code)
        {
            

            string comm = command.ToLower();

            
        }*/
    }
}



