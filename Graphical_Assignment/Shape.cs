﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    public abstract class Shape : ShapeInterface
    {
        protected Color colour;
        protected int x = 0, y = 0;
        public Shape()
        {
            colour = Color.Red;
        }


        public Shape( int x, int y)
        {
/*
            this.colour = colour;*/
            this.x = x;
            this.y = y;

        }


        public abstract void Draw(Graphics g, Color c, int thickness,Brush br);




        public virtual void SetColor(Color colour)
        {
            this.colour = colour;

        }
        public virtual void fillColor(Color colour)
        {
            this.colour = colour;

        }
        public void SetX(int x)
        {
            this.x = x;
        }
        public void SetY(int y)
        {
            this.y = y;
        }

        public int GetX()
        {
            return x;
        }
        public int GetY()
        {
            return y;
        }

        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }
    }
}



