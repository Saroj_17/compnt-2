﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    class Triangle :Shape
    {
        PointF p1, p2, p3;

        public void setPoints(int point1, int point2, int point3, int point4, int point5, int point6)
        {
            this.p1 = new PointF(point1, point2);
            this.p2 = new PointF(point3, point4);
            this.p3 = new PointF(point5, point6);
        }

        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {

            Pen p = new Pen(c, thickness);
            PointF[] curvPoints =
            {
                p1,p2,p3
            };
            g.DrawPolygon(p, curvPoints);
          //  g.FillPolygon(br, curvPoints);

        }
    }
}
