﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    ///Circle class
     class Circle : Shape
    {

        ///declare variables
        int X, Y,rad;


        /// Parameter constructor        
        public Circle(int X, int Y,int Rad) 
        {
            this.X = X;
            this.Y = Y;
            this.rad = Rad;
        }

        /// another parameter constructor        
        public Circle(int x, int y) : base(x, y)
        {

        }

        /// default constructor       
        public Circle()
        {


        }



        /// draw method

        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {
            Pen p = new Pen(c, thickness);
            
            g.DrawEllipse(p, x, y, rad,rad);
         ///   g.FillEllipse(br, x, y, rad, rad);
        }
        ///set radious if circle
        public void SetRadious(int rad)
        {
            this.rad = rad;
        }

        public int getRadious()
        {
            return this.rad;
        }

        
       

    }
}
